1.) 
SELECT customerName FROM customers WHERE country = "Philippines";

2.)
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

3.)
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

4.)
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

5.)
SELECT customerName FROM customers WHERE state IS NULL;

6.)
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

7.)
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

8.)
SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";

9.)
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

10.)
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

11.) 
SELECT DISTINCT country FROM customers;

12.)
SELECT DISTINCT status FROM orders;

13.)
SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

14.)
SELECT employees.firstName, employees.lastName, offices.city FROM employees JOIN offices
ON employees.officeCode = offices.officeCode AND city = "Tokyo";

15.)
SELECT customers.customerName FROM customers JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber
AND employees.firstName = "Leslie" AND employees.lastName = "Thompson";

16.)
SELECT products.productName, customers.customerName FROM customers
JOIN orders ON customers.customerNumber = orders.customerNumber
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
JOIN products ON orderdetails.productCode = products.productCode
AND customers.customerName = "Baane Mini Imports";

17.)
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM employees
JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
JOIN offices ON employees.officeCode = offices.officeCode
AND customers.country = offices.country;

18.)
SELECT lastName, firstName FROM employees WHERE reportsTo = ( 
SELECT employeeNumber FROM employees WHERE firstName = "Anthony" AND lastName = "Bow" );

19.)
SELECT productName, MAX(MSRP) FROM products;

20.)
SELECT COUNT(customerName) AS numberOfCustomers FROM customers WHERE country = "UK";

21.)
SELECT productLine, COUNT(productLine) AS numberOfProductLine FROM products GROUP BY productLine;

22.)
SELECT CONCAT(employees.firstName, ' ', employees.lastName) AS employeeName, COUNT(customers.customerNumber) AS numberOfCustomers FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
GROUP BY customers.salesRepEmployeeNumber;

23.)
SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;

